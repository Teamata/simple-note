import React, { Component } from 'react';
import { auth } from '../firebase';

import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
});

class PasswordReset extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: ""
            }
            this.onSubmit = this.onSubmit.bind(this);
            this.handleChange = this.handleChange.bind(this);
        }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };


    onSubmit(event) {
    event.preventDefault();
    const { email } = this.state;
    auth.sendPasswordResetEmail(email)
        .then(authUser => {
            console.log(email);
            console.log(authUser);
        })
        .catch(authError => {
            alert(authError);
        })
    }

    render(){
        const { email} = this.state;
        const classes = this.props.classes;
        return(
            <Grid container>
                <Grid item xs={12}>
                    <Paper className={classes.paper}>
                        <h1>Password Recovery</h1>
                        <form onSubmit={this.onSubmit} autoComplete="off">
                            <TextField
                                id="email"
                                label="Email"
                                className={classes.textField}
                                value={email}
                                onChange={this.handleChange('email')}
                                margin="normal"
                                type="email"
                            />
                            <br />
                            <Button variant="raised" color="primary" type="submit">reset password</Button>
                        </form>
                    </Paper>
                </Grid>
            </Grid>
        );
    }
}



export default withStyles(styles)(PasswordReset);