import React, { Component } from 'react';
import { auth } from '../firebase';

import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
});

class Profile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: auth.currentUser.displayName,
            email: auth.currentUser.email,
            editMode: true
        };
        this.resetPassword = this.resetPassword.bind(this);
        this.enableEdit = this.enableEdit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.updateEmail = this.updateEmail.bind(this);
        this.updateName = this.updateName.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    resetPassword(){
        auth.sendPasswordResetEmail(auth.currentUser.email)
            .then(authUser => {
                alert("The password reset email have been sent");
            })
            .catch(authError => {
                alert(authError);
            })
    }


    updateEmail(){
        if(auth.currentUser.email !== this.state.email) {
            auth.currentUser.updateEmail(this.state.email)
                .then(updateMail => {
                    alert("The Email change request have been sent");
                })
                .catch(mailError => {
                    alert(mailError)
                })
        }
    }

    updateName(){
        if(auth.currentUser.displayName !== this.state.name) {
            auth.currentUser.updateProfile({
                displayName: this.state.name
            }).then(function () {
                alert("Your name have been updated successfully");
            }, function (error) {
                alert(error);
                // An error happened.
            });
        }
    }

    onSubmit(event) {
        event.preventDefault();
        this.updateName();
        this.updateEmail();
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    enableEdit(){
        this.setState({
            editMode : !this.state.editMode
        })
    }

    render(){
        const classes = this.props.classes;
        const isEditMode = this.state.editMode;
        return ( <Grid container>
                <Grid item xs={12}>
                    <Paper className={classes.paper}>
                        <img src= {auth.currentUser.photoURL}
                             style={{width: 150, height: 150}} />
                        <p>Method : { auth.currentUser.providerData[0].providerId } </p>
                        <p>verify status : {auth.currentUser.emailVerified.valueOf().toLocaleString()} </p>
                        <form onSubmit={this.onSubmit} autoComplete="off">
                            <TextField
                                id="email"
                                label="Email"
                                className={classes.textField}
                                defaultValue={auth.currentUser.email}
                                onChange={this.handleChange('email')}
                                disabled = {this.state.editMode}
                                margin="normal"
                                type="email"
                            />
                            <br />
                            <TextField
                                id="username"
                                label="Username"
                                className={classes.textField}
                                defaultValue={auth.currentUser.displayName}
                                onChange={this.handleChange('name')}
                                disabled = {this.state.editMode}
                                margin="normal"
                                type="text"
                            />
                            <br />
                            <Button variant="raised" color="primary" type="submit" onClick={() => this.enableEdit()}>
                                {isEditMode ? ('edit') : ('submit')}
                            </Button>
                            <ul></ul>
                        </form>
                        <Button  color="primary" type="submit" onClick={() => this.resetPassword()}>reset password</Button>
                    </Paper>
                </Grid>
            </Grid>
        )
    }
}


export default withStyles(styles)(Profile);