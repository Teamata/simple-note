import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import  firebase, { auth } from '../firebase';

import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';


import FaFacebook from 'react-icons/lib/fa/facebook'
import FaGoogle from 'react-icons/lib/fa/google-plus'

const styles = theme => ({
    root: {
        flexGrow: 0.5,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
});

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email : "",
            password : ""
        }
        this.onSubmit = this.onSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.facebookLogin = this.facebookLogin.bind(this);
        this.googleLogin = this.googleLogin.bind(this);
    }

    onSubmit(event) {
        event.preventDefault();
        const { email, password } = this.state;
        auth.signInWithEmailAndPassword(email, password)
            .then(authUser => {
                console.log(authUser);
                if(auth.currentUser.emailVerified === false){
                    alert("please verify your email!");
                }
            })
            .catch(authError => {
                alert(authError);
            })
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };


    facebookLogin(){
        const provider = new firebase.auth.FacebookAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) {
            // This gives you a Facebook Access Token. You can use it to access the Facebook API.
            // var token = result.credential.accessToken;
            // The signed-in user info.
            // var user = result.user;
            // console.log(user);
            // console.log(token);
            // ...
        }).catch(function(error) {
            // Handle Errors here.
            // var errorCode = error.code;
            // var errorMessage = error.message;
            // console.error(errorMessage);
            // The email of the user's account used.
            // var email = error.email;
            // console.error(email);
            // The firebase.auth.AuthCredential type that was used.
            // var credential = error.credential;
            // ...
        });
    }

    googleLogin(){
        const provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) {
            // This gives you a Google Access Token. You can use it to access the Google API.
            // var token = result.credential.accessToken;
            // The signed-in user info.
            // var user = result.user;
            // ...
        }).catch(function(error) {
            // Handle Errors here.
            // var errorCode = error.code;
            // var errorMessage = error.message;
            // The email of the user's account used.
            // var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            // var credential = error.credential;
            // ...
        });
    }

    //wraps around grid items

    render() {
        const { email, password } = this.state;
        const classes = this.props.classes;
        return (
            <Grid container>
                <Grid item xs={12}>
                    <Paper className={classes.paper}>
                        <h1>Log in</h1>
                        <form onSubmit={this.onSubmit} autoComplete="off">
                            <TextField
                                id="email"
                                label="Email"
                                className={classes.textField}
                                value={email}
                                onChange={this.handleChange('email')}
                                margin="normal"
                                type="email"
                            />
                            <br />
                            <TextField
                                id="password"
                                label="Password"
                                className={classes.textField}
                                value={password}
                                onChange={this.handleChange('password')}
                                margin="normal"
                                type="password"
                            />
                            <br />
                            <Button variant="raised" color="primary" type="submit">Log in</Button>
                            <ul></ul>
                        </form>
                        <Button variant="raised" color="primary" type="submit" onClick={() => this.facebookLogin()}>
                                          <FaFacebook size={20} /></Button>
                        <Button variant="raised" color="#D84B37" type="submit" onClick={() => this.googleLogin()} >
                        <FaGoogle size={20} color="#D84B37" /> </Button>
                        <p>Don't have an account? <Link to="/signup">Sign up here</Link></p>
                        <p><Link to="/pwdreset">Forgot password?</Link></p>
                    </Paper>
                </Grid>
            </Grid>
        );
    }
}

export default withStyles(styles)(Login);