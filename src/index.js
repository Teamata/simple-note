import React from 'react';
import ReactDOM from 'react-dom';

import App from './components/App';
import {BrowserRouter} from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';



//root is defined in public/index.html
ReactDOM.render(
    //allow app to interact with url bar
    <BrowserRouter>
    <App />
    </BrowserRouter>,
    document.getElementById('root'));
registerServiceWorker();
