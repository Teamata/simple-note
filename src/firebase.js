import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyCnKA2j6zBIUhb_Omlb5lEePNSTCn4IYUg",
    authDomain: "simplenote-5cc14.firebaseapp.com",
    databaseURL: "https://simplenote-5cc14.firebaseio.com",
    projectId: "simplenote-5cc14",
    storageBucket: "simplenote-5cc14.appspot.com",
    messagingSenderId: "741291616345"
};

firebase.initializeApp(config);

export default firebase;
export const db = firebase.database();
export const auth = firebase.auth();